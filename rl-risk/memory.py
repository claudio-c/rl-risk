import torch
import dataclasses


@dataclasses.dataclass
class Event:
    local_state: torch.Tensor
    global_state: torch.Tensor
    action: int
    reward: float
    maxq: float


class ReplayMemory:
    def __init__(self, capacity, territories, players, global_size, batch_size):
        self.sz = 0
        self.index = 0
        self.local = torch.empty(capacity, territories, players)
        self.global_ = torch.empty(capacity, global_size)
        self.action = torch.empty(capacity, dtype=int)
        self.reward = torch.empty(capacity)
        self.maxq = torch.empty(capacity)
        self.batch_size = batch_size

    def add(self, exp):
        self.local[self.index, :, :] = exp.local_state[0, :, :]
        self.global_[self.index, :] = exp.global_state[0, :]
        self.action[self.index] = torch.tensor(exp.action)
        self.reward[self.index] = torch.tensor(exp.reward)
        self.maxq[self.index] = torch.tensor(exp.maxq)
        self.index = (self.index + 1) % self.action.numel()
        self.sz = min(self.sz + 1, self.action.numel())

    def sample(self):
        idx = torch.randint(0, self.sz, (self.batch_size,))
        return (self.local[idx, ...], self.global_[idx], self.action[idx],
                self.reward[idx], self.maxq[idx])
