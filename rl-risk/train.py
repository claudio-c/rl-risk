#!/usr/bin/env python3

import argparse
import random
import torch
import mapdata
import risk
import aiplayer
import memory


def parse_args():
    parser = argparse.ArgumentParser(description="Train the Risk AI")
    a = parser.add_argument
    a("-m", "--map", default="standard.yaml", dest="map_", help="map yaml file")
    a("-I", "--image-dir", default="images", help="image directory")
    a("-l", "--learning-rate", type=float, default=1e-4, help="Learning rate")
    a("-b", "--batch-size", type=int, default=64, help="Batch size")
    a("-f", "--features", type=int, default=32, help="size of feature vectors")
    a("-D", "--depth", type=int, default=2, help="depth of the model")
    a("-L", "--max-length", type=int, default=3000, help="Maximum simulation length")
    a("-e", "--epsilon", type=float, default=0.1, help="Exploration probability")
    a("-r", "--replay-memory", type=int, default=100000, help="Replay memory size")
    a("-s", "--steps", type=int, default=1000, help="Training steps for each simulation")
    a("-i", "--iterations", type=int, default=1000, help="Training iterations")
    a("-g", "--gamma", type=float, default=0.99, help="Discount factor")
    a("-R", "--start-random", type=int, default=12, help="Initial random actions")
    a("-S", "--save-every", type=int, default=100, help="Save every n games")
    a("-n", "--save-name", default="ai-{n:04d}.pt", help="Output file name")
    a("--death-penalty", type=float, default=10, help="Penalty for players who die")
    a("--territory-reward", type=float, default=1, help="Bouns for each territory owned")
    dev = ("cuda" if torch.cuda.is_available() else "cpu")
    a("-d", "--device", default=dev, help="Computing device")
    return parser.parse_args()


def simulation(map_, ai, length, epsilon, start_random, device, death_penalty=1, territory_reward=0):
    nplayers = random.choice(map_.number_of_players())
    game = risk.Risk(map_)
    game.start_game(nplayers)
    trace = [[] for _ in range(nplayers)]
    for turn in range(length):
        alive = game.alive.copy()
        player = game.current_player
        if alive.sum() < 2:
            break
        # Use the AI to select a move (epsilon-greedy policy)
        local, global_ = aiplayer.game_status(game)
        with torch.no_grad():
            scores = ai(local.to(device), global_.to(device)).squeeze(0).to("cpu")
        va = game.valid_actions()
        scores = torch.where(torch.tensor(va), scores, torch.tensor(-float("inf")))
        if random.random() < epsilon or turn < start_random:
            action = random.choice(va.nonzero()[0])
        else:
            action = scores.argmax().item()
        # Perform the action and record the event
        terrs_before = game.count_territories()
        game.take_action(action)
        terrs_after = game.count_territories()
        event = memory.Event(local, global_, action, 0, 0)
        if trace[player]:
            trace[player][-1].maxq = scores.max().item()
        trace[player].append(event)
        # Adjust the rewards for the territories gained or lost.
        if (terrs_before != terrs_after).any():
            for p in range(terrs_before.size):
                if trace[p]:
                    diff = terrs_after[p] - terrs_before[p]
                    trace[p][-1].reward += territory_reward * diff
        # If someone died, set the reward for the last action
        # performed by the players
        if alive.sum() > game.alive.sum():
            for p in range(alive.size):
                if game.alive[p] and trace[p]:
                    trace[p][-1].reward += death_penalty
                elif alive[p] and trace[p]:
                    trace[p][-1].reward -= death_penalty * game.alive.sum()
    return [e for t in trace for e in t]


def train(ai, mem, optimizer, steps, device, gamma):
    idx = torch.arange(mem.batch_size, device=device)
    totloss = 0
    for _ in range(steps):
        data = mem.sample()
        local, global_, action, reward, maxq = (x.to(device) for x in data)
        optimizer.zero_grad()
        scores = ai(local, global_)
        tgt = reward + gamma * maxq
        q = scores[idx, action]
        loss = ((q - tgt) ** 2).mean()
        totloss += loss.item()
        loss.backward()
        optimizer.step()
    return totloss / steps


def main():
    args = parse_args()
    for k, v in vars(args).items():
        print(f"{k:12}: {v}")
    print()
    map_ = mapdata.RiskMap.load(args.map_, args.image_dir)
    ai = aiplayer.AIPlayer(args.features, args.depth, map_=map_)
    ai.to(args.device)
    optimizer = torch.optim.Adam(ai.parameters(), lr=args.learning_rate)
    mem = memory.ReplayMemory(args.replay_memory, len(map_.territories),
                              map_.max_players(), aiplayer.GLOBAL_STATUS_LEN,
                              args.batch_size)

    for game in range(1, args.iterations + 1):
        ai.eval()
        events = simulation(map_, ai, args.max_length, args.epsilon,
                            args.start_random, args.device,
                            death_penalty=args.death_penalty,
                            territory_reward=args.territory_reward)
        print(f"Game {game}: {len(events)} actions \t", end="", flush=True)
        for e in events:
            mem.add(e)
        del events[:]

        ai.train()
        loss = train(ai, mem, optimizer, args.steps, args.device, args.gamma)
        print(f"Training: loss = {loss:.4f}")
        if game % args.save_every == 0:
            ai.save(args.save_name.format(n=game),
                    optimizer=optimizer.state_dict(),
                    args=args)


if __name__ == "__main__":
    main()
