import yaml
import matplotlib.pyplot as plt
import os


COLORS = [
    "#e31a1c",  # RED
    "#1f78b4",  # BLUE
    "#33a02c",  # GREEN
    "#fccde5",  # PINK
    "#ffffb3",  # YELLOW
    "#969696",  # BLACK (actually gray)
]


class RiskMap:
    def __init__(self):
        self.territories = []
        self.borders = []
        self.continents = []
        self.in_continent = []
        self.bonus = []
        self.initial_armies = {}
        self.coordinates = []
        self.image = None
        self.deck = []
        self.bonus_set = [0]
        self.bonus_territory = 0

    def plot(self, players, armies, current=-1):
        xs, ys = zip(*self.coordinates)
        plt.clf()
        plt.imshow(self.image)
        plt.axis('off')
        s = [min(x, 20) * 75 for x in armies]
        c = [COLORS[p] for p in players]
        plt.scatter(xs, ys, c=c, s=s)
        plt.scatter([x for x, a in zip(xs, armies) if a == 0],
                    [y for y, a in zip(ys, armies) if a == 0],
                    c="#ffffff", s=75)
        n = players.max() + 1
        plt.scatter([20 * i for i in range(1, n + 1)],
                    [10] * n,
                    c=[COLORS[i] for i in range(n)],
                    s=[(100 if i == current else 20) for i in range(n)])
        for a, xy in zip(armies, self.coordinates):
            if a > 0:
                plt.annotate(str(a), xy, va="center", ha="center")

    def number_of_players(self):
        return sorted(self.initial_armies)

    def max_players(self):
        return max(self.initial_armies)

    @classmethod
    def load(cls, filename, imagedir="."):
        obj = cls()
        with open(filename) as f:
            data = yaml.load(f, Loader=yaml.Loader)
        obj.territories = [t for ts in data["territories"].values() for t in ts]
        obj.territories.sort()
        idx = {name: n for n, name in enumerate(obj.territories)}
        obj.borders = [(idx[from_], idx[to])
                       for from_, tos in data["borders"].items() for to in tos]
        obj.borders.sort()
        obj.continents = sorted(data["continents"])
        obj.bonus = [data["continents"][c] for c in obj.continents]
        obj.in_continent = [None] * len(obj.territories)
        for c, ts in data["territories"].items():
            ic = obj.continents.index(c)
            for t in ts:
                obj.in_continent[idx[t]] = ic
        obj.initial_armies = dict(data["initial_armies"])
        fname = os.path.join(imagedir, data["map"]["image"])
        obj.image = plt.imread(fname)
        obj.coordinates = [(0, 0)] * len(obj.territories)
        for t, l in data["map"]["coordinates"].items():
            x, y = l
            obj.coordinates[idx[t]] = (x, y)
        obj.deck = list(map(tuple, data["cards"]))
        obj.bonus_set = data["bonus_set"]
        obj.bonus_territory = data["bonus_territory"]
        return obj


def _test():
    import sys
    fname = (sys.argv + ["standard.yaml"])[1]
    map_ = RiskMap.load(fname, imagedir="images")
    print(f"{len(map_.territories)} territories " +
          f"in {len(map_.continents)} continents")
    np = map_.number_of_players()[-1]
    print(map_.number_of_players())
    p = [n % np for n in range(len(map_.territories))]
    a = list(range(len(map_.territories)))
    map_.plot(p, a)
    plt.show()
    sys.exit()


if __name__ == "__main__":
    _test()
