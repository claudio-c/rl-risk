#!/usr/bin/env python3

import argparse
import random
import torch
import matplotlib.pyplot as plt
import mapdata
import risk
import aiplayer


def parse_args():
    parser = argparse.ArgumentParser(description="Train the Risk AI")
    a = parser.add_argument
    a("ai_model", nargs="+", help="AI model files")
    a("-m", "--map", default="standard.yaml", dest="map_", help="map yaml file")
    a("-I", "--image-dir", default="images", help="image directory")
    dev = ("cuda" if torch.cuda.is_available() else "cpu")
    a("-d", "--device", default=dev, help="Computing device")
    a("-n", "--players", type=int, default=0, help="Number of players")
    a("-r", "--random", type=int, default=5, help="Starting random actions")
    return parser.parse_args()


def make_action_labels(map_):
    labels = map_.territories[:]
    labels.extend(map_.territories[from_] + " -> " + map_.territories[to]
                  for from_, to in map_.borders)
    labels.append("STOP")
    return labels


def play_game(map_, players, nrandom, device):
    game = risk.Risk(map_)
    game.start_game(len(players))
    action_labels = make_action_labels(map_)

    plt.ion()
    plt.figure(1)
    plt.figure(2)

    for turn in range(1000000):
        if game.alive.sum() < 2:
            break
        player = game.current_player

        local, global_ = aiplayer.game_status(game)
        with torch.no_grad():
            scores = players[player](local.to(device), global_.to(device)).squeeze(0).to("cpu")
        va = game.valid_actions()
        scores = torch.where(torch.tensor(va), scores, torch.tensor(-float("inf")))
        action = scores.argmax().item()
        if turn < nrandom:
            action = random.choice([i for i, v in enumerate(va) if v])
            
        plt.figure(1)
        plt.clf()
        plt.title("Scores")
        va_scores = scores[va]
        plt.barh(torch.arange(va_scores.numel()), va_scores)
        ax = plt.gca()
        ax.set_yticks(range(va.sum()))
        ax.set_yticklabels([a for a, v in zip(action_labels, va) if v])

        msg = f"{turn:4d} PLAYER {player}: {action_labels[action]} ({scores[action]})"
        print(msg)
        game.take_action(action)

        plt.figure(2)
        map_.plot(game.owner, game.armies, player)
        plt.annotate(msg, (30, 30), va="center", ha="left")
        plt.pause(0.001)
    winner = game.alive.argmax()
    print(f"PLAYER {winner} wins")
    plt.ioff()
    plt.show()


def main():
    args = parse_args()
    print("Setup")
    map_ = mapdata.RiskMap.load(args.map_, args.image_dir)
    models = [aiplayer.AIPlayer.load(m, device=args.device) for m in args.ai_model]
    n = (args.players if args.players > 0 else len(models))
    if n not in map_.number_of_players():
        print(f"The map does not support that many ({n}) players")
        return
    players = [models[i % len(models)] for i in range(n)]
    print("OK")
    play_game(map_, players, args.random * n, args.device)


if __name__ == "__main__":
    main()
