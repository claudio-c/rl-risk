#!/usr/bin/env python3

import mapdata
import numpy as np
import matplotlib.pyplot as plt
import argparse


def parse_args():
    desc = "Compute and show the Laplacian of a map"
    parser = argparse.ArgumentParser(description=desc)
    a = parser.add_argument
    a("map_file", help="map yaml file")
    a("-i", "--image-dir", default="images", help="Image directory")
    a("-n", "--normalized", action="store_true",
      help="Compute normalized laplacian")
    a("-d", "--display", type=int, default=10,
      help="Number of features to display")
    a("-k", "--clusters", type=int, default=0,
      help="Find the given number of clusters")
    return parser.parse_args()


def compute_laplacian(map_, normalized=False):
    n = len(map_.territories)
    W = np.zeros((n, n))
    for from_, to in map_.borders:
        W[from_, to] = 1
    D = W.sum(0)
    if normalized:
        N = np.diag(1 / np.sqrt(D))
        L = np.eye(n) - N @ W @ N
    else:
        L = np.diag(D) - W
    return L


def spectral_analysis(L):
    evals, evecs = np.linalg.eigh(L)
    return evals, evecs


def plot_fun(map_, x):
    xs, ys = zip(*map_.coordinates)
    plt.imshow(map_.image)
    plt.axis('off')
    val = np.abs(x).max()
    plt.scatter(xs, ys, c=x, s=200, cmap="seismic", vmin=-val, vmax=val)
    for z, xy in zip(x, map_.coordinates):
        plt.annotate(f"{z:.2g}", xy, va="center", ha="center")


def plot_clusters(map_, clusters):
    xs, ys = zip(*map_.coordinates)
    plt.imshow(map_.image)
    plt.axis('off')
    plt.scatter(xs, ys, c=clusters, s=200, cmap="hsv")


def clustering(map_, features, k, maxsteps=1000):
    features = features / (features ** 2).sum(1, keepdims=True)
    m, n = features.shape
    lbls = np.arange(m) % k
    np.random.shuffle(lbls)
    dists = np.empty((m, k))
    for _ in range(maxsteps):
        old = lbls[:]
        for c in range(k):
            centroid = features[lbls == c].mean(0)
            dists[:, c] = ((features - centroid) ** 2).sum(1)
        lbls = dists.argmin(1)
        if np.all(old == lbls):
            break
    return lbls


def main():
    args = parse_args()
    map_ = mapdata.RiskMap.load(args.map_file, imagedir=args.image_dir)
    L = compute_laplacian(map_, args.normalized)
    evals, evecs = spectral_analysis(L)
    plt.plot(evals)
    for i in range(min(evals.size, args.display)):
        plt.figure()
        plot_fun(map_, evecs[:, i])
        plt.title(str(i))
    if args.clusters > 0:
        clusters = clustering(map_, evecs[:, :args.clusters], args.clusters)
        plt.figure()
        plot_clusters(map_, clusters)
        plt.title("Clustering")
    plt.show()


if __name__ == "__main__":
    main()
