import random
import mapdata
import numpy as np
import itertools


# GAME PHASES
IDLE_PHASE = "IDLE"  # Game is not started yet
CLAIM_PHASE = "CLAIM"  # Occupy the territories
DISTRIBUTION_PHASE = "DISTRIBUTION"  # Place armies on the claimed territories
REINFORCEMENT_PHASE = "REINFORCEMENT"  # Place new armies
ATTACK_PHASE = "ATTACK"  # Attack territories
OCCUPATION_PHASE = "OCCUPATION"  # Move to a conquered territory
MOVE_PHASE = "MOVE"  # Move between owned territories


# TODO:
# - missions


class phase_method:
    """State pattern to manage game phases."""
    def __init__(self):
        self.funs = {}

    def __get__(self, obj, objclass=None):
        g = self.funs[obj.phase]
        return lambda *args, **kwargs: g(obj, *args, **kwargs)

    def phase(self, phase):
        """Decorator that register a method specific for the phase."""
        def f(fun):
            self.funs[phase] = fun
            return self
        return f


def roll_dice(n):
    """Rool the given number of dice."""
    return [random.randrange(1, 7) for _ in range(n)]


def attack_result(adice, ddice):
    """Roll the dice and return the outcome (dice, wins, losses)."""
    att = roll_dice(adice)
    att.sort(reverse=True)
    def_ = roll_dice(ddice)
    def_.sort(reverse=True)
    wins = sum(a > d for a, d in zip(att, def_))
    losses = min(adice, ddice) - wins
    return (att, def_), wins, losses


class Risk:
    """A game of risk on the given map."""
    def __init__(self, map_):
        self.map_ = map_
        self.nplayers = 2
        self.phase = IDLE_PHASE
        max_players = map_.max_players()
        self.owner = -np.ones(len(map_.territories), dtype=int)
        self.armies = np.zeros(len(map_.territories), dtype=int)
        self.current_player = 0
        self.reinforcements = np.zeros(max_players, dtype=int)
        self.alive = np.zeros(max_players, dtype=bool)
        self.actions = len(map_.territories) + len(map_.borders) + 1
        self.move = None
        self.conquered = np.zeros(max_players, dtype=int)  # Terr. taken in the turn
        self.deck = map_.deck[:]
        self.hands = [[] for _ in range(self.nplayers)]
        self.set_converted = 0

    def start_game(self, nplayers):
        """Start a game with nplayers players."""
        self.nplayers = nplayers
        self.owner[:] = 0
        self.armies[:] = 0
        self.reinforcements[:] = 0
        self.reinforcements[:nplayers] = self.map_.initial_armies[nplayers]
        self.current_player = random.randrange(nplayers)
        self.alive[:] = (np.arange(self.map_.max_players()) < nplayers)
        self.deck = self.map_.deck[:]
        random.shuffle(self.deck)
        self.hands = [[] for _ in range(self.nplayers)]
        self.set_converted = 0
        self.phase = CLAIM_PHASE

    take_action = phase_method()

    @take_action.phase(CLAIM_PHASE)
    def _(self, action):
        if action >= self.owner.size:
            raise ValueError("Invalid action (must be a territory)")
        if self.armies[action] > 0:
            raise ValueError("Invalid action (territory already claimed)")
        if self.reinforcements[self.current_player] < 1:
            raise ValueError("Claiming without armies")
        self.armies[action] = 1
        self.owner[action] = self.current_player
        self.reinforcements[self.current_player] -= 1
        self.next_player()
        if self.armies.min() > 0:
            self.phase = DISTRIBUTION_PHASE

    @take_action.phase(DISTRIBUTION_PHASE)
    def _(self, action):
        if action >= self.owner.size:
            raise ValueError("Invalid action (must be a territory)")
        if self.owner[action] != self.current_player:
            raise ValueError("Invalid action (cannot place armies on other players' territories)")
        if self.reinforcements[self.current_player] < 1:
            raise ValueError("No armies left")
        self.armies[action] += 1
        self.reinforcements[self.current_player] -= 1
        self.next_player()
        if self.reinforcements.max() == 0:
            self.phase = REINFORCEMENT_PHASE
            self.assign_reinforcements()

    def assign_reinforcements(self):
        self.convert_sets()
        self.conquered[self.current_player] = 0
        n = (self.owner == self.current_player).sum() // 3
        cs = np.array(self.map_.in_continent, dtype=int)
        opponents = ~(self.owner == self.current_player)
        for k, bonus in enumerate(self.map_.bonus):
            if not np.any((cs == k) & opponents):
                n += bonus
        self.reinforcements[self.current_player] += n
        if n == 0:
            self.phase = ATTACK_PHASE

    def convert_sets(self):
        while True:
            cards = self.hands[self.current_player]
            for a, b, c in itertools.combinations(cards[:], 3):
                figures = set((a[1], b[1], c[1]))
                if None in figures or len(figures) != 2:
                    index = min(self.set_converted, len(self.map_.bonus_set) - 1)
                    self.reinforcements[self.current_player] += self.map_.bonus_set[index]
                    self.set_converted += 1
                    for cc in (a, b, c):
                        self.hands[self.current_player].remove(cc)
                        if cc[0] is not None:
                            index = self.map_.territories.index(cc[0])
                            if self.owner[index] == self.current_player:
                                bonus = self.map_.bonus_territory
                                self.reinforcements[self.current_player] += bonus
                    break
            else:
                return

    @take_action.phase(REINFORCEMENT_PHASE)
    def _(self, action):
        if action >= self.owner.size:
            raise ValueError("Invalid action (must be a territory)")
        if self.owner[action] != self.current_player:
            raise ValueError("Invalid action (cannot place armies on other players' territories)")
        if self.reinforcements[self.current_player] < 1:
            raise ValueError("No armies left")
        self.armies[action] += 1
        self.reinforcements[self.current_player] -= 1
        if self.reinforcements[self.current_player] == 0:
            self.move = None
            self.phase = ATTACK_PHASE

    @take_action.phase(ATTACK_PHASE)
    def _(self, action):
        if action < self.owner.size:
            raise ValueError("Invalid action (must be a border or stop)")
        if action == self.actions - 1:
            self.move = None
            self.phase = MOVE_PHASE
            return
        border = action - self.owner.size
        from_, to = self.map_.borders[border]
        if self.owner[from_] != self.current_player:
            raise ValueError("Attack must come from an owned territory")
        if self.armies[from_] < 2:
            raise ValueError("Need at least two armies to attack")
        if self.owner[to] == self.current_player:
            raise ValueError("Cannot attack an already occupied territory")
        adice = min(3, self.armies[from_] - 1)
        ddice = min(2, self.armies[to])
        _, wins, losses = attack_result(adice, ddice)
        self.armies[from_] -= losses
        self.armies[to] -= wins
        if self.armies[to] == 0:
            # TODO: Check win
            self.armies[to] = 1
            self.armies[from_] -= 1
            self.conquered[self.current_player] += 1
            if (self.owner == self.owner[to]).sum() == 1:
                self.alive[self.owner[to]] = False
                self.hands[self.current_player].extend(self.hands[self.owner[to]])
                self.hands[self.owner[to]] = []
            self.owner[to] = self.current_player
            if self.armies[from_] > 1:
                self.move = border
                self.phase = OCCUPATION_PHASE

    @take_action.phase(OCCUPATION_PHASE)
    def _(self, action):
        if action < self.owner.size:
            raise ValueError("Invalid action (must be a border or stop)")
        if action == self.actions - 1:
            self.move = None
            self.phase = ATTACK_PHASE
            return
        border = action - self.owner.size
        if border != self.move:
            raise ValueError("Must move from the attacker to the occupied territory")
        from_, to = self.map_.borders[border]
        self.armies[to] += 1
        self.armies[from_] -= 1
        if self.armies[from_] == 1:
            self.move = None
            self.phase = ATTACK_PHASE

    @take_action.phase(MOVE_PHASE)
    def _(self, action):
        if action < self.owner.size:
            raise ValueError("Invalid action (must be a border or stop)")
        if action == self.actions - 1:
            # Assign a card if earned
            if self.conquered[self.current_player] > 0:
                if not self.deck:
                    self.deck = self.map_.deck[:]
                    for h in self.hands:
                        for c in h:
                            self.deck.remove(c)
                    random.shuffle(self.deck)
                self.hands[self.current_player].append(self.deck.pop())
            self.next_player()
            self.phase = REINFORCEMENT_PHASE
            self.assign_reinforcements()
            return
        border = action - self.owner.size
        if self.move is not None and border != self.move:
            raise ValueError("There is already a different move")
        from_, to = self.map_.borders[border]
        if self.owner[from_] != self.current_player:
            raise ValueError("Must move from an occupied territories")
        if self.armies[from_] < 2:
            raise ValueError("Need at least two armies to move")
        if self.owner[to] != self.current_player:
            raise ValueError("Must move to an occupied territories")
        self.move = border
        self.armies[from_] -= 1
        self.armies[to] += 1
        if self.armies[from_] == 1:
            self.next_player()
            self.phase = REINFORCEMENT_PHASE
            self.assign_reinforcements()

    def next_player(self):
        """Switch to the next active player."""
        while True:
            self.current_player = (self.current_player + 1) % self.nplayers
            if self.alive[self.current_player]:
                break

    valid_actions = phase_method()

    @valid_actions.phase(IDLE_PHASE)
    def _(self):
        return np.zeros(self.actions, dtype=bool)

    @valid_actions.phase(CLAIM_PHASE)
    def _(self):
        va = np.zeros(self.actions, dtype=bool)
        va[:self.armies.size] = (self.armies == 0)
        return va

    @valid_actions.phase(DISTRIBUTION_PHASE)
    def _(self):
        va = np.zeros(self.actions, dtype=bool)
        va[:self.armies.size] = (self.owner == self.current_player)
        return va

    @valid_actions.phase(REINFORCEMENT_PHASE)
    def _(self):
        va = np.zeros(self.actions, dtype=bool)
        va[:self.armies.size] = (self.owner == self.current_player)
        return va

    @valid_actions.phase(ATTACK_PHASE)
    def _(self):
        borders = np.array(self.map_.borders, dtype=int)
        vb = ((self.owner[borders[:, 0]] == self.current_player) &
              (self.armies[borders[:, 0]] > 1) &
              (self.owner[borders[:, 1]] != self.current_player))
        va = np.zeros(self.actions, dtype=bool)
        va[self.owner.size:-1] = vb
        va[-1] = True
        return va

    @valid_actions.phase(OCCUPATION_PHASE)
    def _(self):
        va = np.zeros(self.actions, dtype=bool)
        va[-1] = True
        if self.move is not None and self.armies[self.map_.borders[self.move][0]] > 1:
            va[self.move + self.owner.size] = True
        return va

    @valid_actions.phase(MOVE_PHASE)
    def _(self):
        va = np.zeros(self.actions, dtype=bool)
        va[-1] = True
        if self.move is None:
            borders = np.array(self.map_.borders, dtype=int)
            vb = ((self.owner[borders[:, 0]] == self.current_player) &
                  (self.armies[borders[:, 0]] > 1) &
                  (self.owner[borders[:, 1]] == self.current_player))
            va[self.owner.size:-1] = vb
        elif self.armies[self.map_.borders[self.move][0]] > 1:
            va[self.move + self.owner.size] = True
        return va

    def action_text(self, action):
        """Text representation of the given action."""
        if action < self.owner.size:
            txt = self.map_.territories[action]
        elif action == self.actions - 1:
            txt = "STOP"
        else:
            from_, to = self.map_.borders[action - self.owner.size]
            txt = self.map_.territories[from_] + " ==> " + self.map_.territories[to]
        return f"PLAYER {self.current_player} {self.phase} {txt}"

    def count_territories(self):
        """Return the number of territories owned by each player."""
        c = np.bincount(self.owner + 1, minlength=self.nplayers + 1)[1:]
        return c


def _test():
    map_ = mapdata.RiskMap.load("standard.yaml", "images")
    # map_ = mapdata.RiskMap.load("mini.yaml", "images")
    risk = Risk(map_)
    risk.start_game(map_.max_players())
    # mapdata.plt.ion()
    turn = 0
    while risk.alive.sum() > 1:
        turn += 1
        if turn % 100 == 0:
            print(turn, np.bincount(risk.owner))
        va = risk.valid_actions().nonzero()[0]
        action = np.random.choice(va)
        # print(risk.action_text(action))
        risk.take_action(action)
        # map_.plot(risk.owner, risk.armies)
        # mapdata.plt.pause(0.01)
    # mapdata.plt.ioff()
    map_.plot(risk.owner, risk.armies)
    mapdata.plt.show()


if __name__ == "__main__":
    _test()
