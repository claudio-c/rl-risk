import torch
import mapdata


_PHASES = {
    "CLAIM": 0,
    "DISTRIBUTION": 1,
    "REINFORCEMENT": 2,
    "ATTACK": 3,
    "OCCUPATION": 4,
    "MOVE": 5
}


GLOBAL_STATUS_LEN = 8


def game_status(game):
    p = game.owner
    a = torch.tensor(game.armies, dtype=torch.float32)
    cur = game.current_player
    maxp = game.map_.max_players()
    local = torch.zeros(1, p.size, maxp)
    local.data[0, torch.arange(p.size), (p - cur) % maxp] = a
    # GLOBAL STATE:
    #   0-5:  one-hot encoded phase
    #   6: reinforcements
    #   7: territories conquered
    global_ = torch.zeros(1, GLOBAL_STATUS_LEN)
    global_.data[0, _PHASES[game.phase]] = 1
    global_.data[0, 6] = game.reinforcements[cur]
    global_.data[0, 7] = game.conquered[cur]
    return local, global_


class AIPlayer(torch.nn.Module):
    """Model for the artificial Risk player.

    Input:
        local: (NxTxP) N batch size, T territories, P players
        global_: (NxG)  N batch size, G global features
    Output:
        (Nx1): stop action
        (NxT): select territory actions
        (NxB): select border actions
    """
    def __init__(self, features, depth, nplayers=None, nterritories=None, nborders=None,
                 map_=None):
        super().__init__()
        g = GLOBAL_STATUS_LEN
        if map_ is not None:
            nplayers = map_.max_players()
            nterritories = len(map_.territories)
            nborders = len(map_.borders)
        borders = [(0, 0) for _ in range(nborders)]
        # Adjacency matrix
        adj = torch.zeros(nterritories, nterritories)
        self.register_buffer("adj", adj)
        # Input adaptation
        emb = 0.1 * torch.randn(nterritories, features)
        self.territory_embedding = torch.nn.Parameter(emb)
        self.local_embedding = torch.nn.Linear(nplayers, features, bias=False)
        self.global_embedding = torch.nn.Linear(g, features, bias=False)
        self.local_norm = torch.nn.LayerNorm(features)
        self.global_norm = torch.nn.LayerNorm(features)
        # Residual blocks
        self.blocks = torch.nn.ModuleList([GNNBlock(features) for _ in range(depth)])
        # Output heads
        self.global_output = torch.nn.Linear(features, 1, bias=False)
        self.territory_output = torch.nn.Linear(features, 1, bias=False)
        self.border_output = BorderHead(features, borders)
        self._setup_data = (features, depth, nplayers, nterritories, nborders)
        # Load map data
        if map_ is not None:
            self.set_map(map_)

    def set_map(self, map_):
        # Adjacency matrix
        self.adj.zero_()
        for from_, to in map_.borders:
            self.adj[from_, to] = 1
        # Set borders
        ifrom = torch.tensor([f for f, t in map_.borders]).long()
        self.border_output.ifrom[:] = ifrom
        ito = torch.tensor([t for f, t in map_.borders]).long()
        self.border_output.ito[:] = ito

    def forward(self, x_local, x_global):
        x_local = self.local_embedding(x_local) + self.territory_embedding
        x_local = torch.relu(self.local_norm(x_local))
        x_global = self.global_embedding(x_global)
        x_global = torch.relu(self.local_norm(x_global))
        for block in self.blocks:
            x_local, x_global = block(x_local, x_global, self.adj)
        x_borders = self.border_output(x_local)
        x_global = self.global_output(x_global)
        x_local = self.territory_output(x_local).squeeze(2)
        return torch.cat((x_local, x_borders, x_global), 1)

    def save(self, filename, **extra):
        data = {
            "state_dict": self.state_dict(),
            "setup": self._setup_data
        }
        data.update(extra)
        torch.save(data, filename)

    @classmethod
    def load(cls, filename, device=None, extra=None):
        data = torch.load(filename, map_location=device)
        instance = cls(*data.pop("setup"))
        if device is not None:
            instance.to(device)
        instance.load_state_dict(data.pop("state_dict"))
        if extra is not None:
            extra.update(data)
        return instance


class BorderHead(torch.nn.Module):
    def __init__(self, features, borders):
        super().__init__()
        self.F = torch.nn.Linear(features, features)
        self.T = torch.nn.Linear(features, features, bias=False)
        self.A = torch.nn.Linear(features, 1, bias=False)
        self.norm = torch.nn.LayerNorm(features)
        ifrom = torch.tensor([f for f, t in borders]).long()
        self.register_buffer("ifrom", ifrom)
        ito = torch.tensor([t for f, t in borders]).long()
        self.register_buffer("ito", ito)

    def forward(self, x):
        xfrom = self.F(x)
        xto = self.T(x)
        y = xfrom[:, self.ifrom, :] + xto[:, self.ito, :]
        y = torch.relu(self.norm(y))
        y = torch.relu(self.A(y)).squeeze(2)
        return y


class GNNBlock(torch.nn.Module):
    def __init__(self, features):
        super().__init__()
        self.gnn1 = GNN(features)
        self.local_norm1 = torch.nn.LayerNorm(features)
        self.global_norm1 = torch.nn.LayerNorm(features)
        self.gnn2 = GNN(features)
        self.local_norm2 = torch.nn.LayerNorm(features)
        self.global_norm2 = torch.nn.LayerNorm(features)

    def forward(self, x_local, x_global, adj):
        y_local, y_global = self.gnn1(x_local, x_global, adj)
        y_local = torch.relu(self.local_norm1(y_local))
        y_global = torch.relu(self.global_norm1(y_global))
        y_local, y_global = self.gnn2(y_local, y_global, adj)
        y_local = x_local + y_local
        y_global = x_global + y_global
        y_local = torch.relu(self.local_norm2(y_local))
        y_global = torch.relu(self.global_norm2(y_global))
        return y_local, y_global


class GNN(torch.nn.Module):
    def __init__(self, features):
        super().__init__()
        self.A = torch.nn.Linear(features, features, bias=False)
        self.B = torch.nn.Linear(features, features, bias=False)
        self.C = torch.nn.Linear(features, features, bias=False)
        self.D = torch.nn.Linear(features, features, bias=False)
        self.E = torch.nn.Linear(features, features, bias=False)

    def forward(self, x_local, x_global, adj):
        mu = x_local.mean(1)
        neighbors = torch.matmul(adj, x_local)
        x_local = self.C(x_global).unsqueeze(1) + self.B(x_local) + self.A(neighbors)
        x_global = self.D(x_global) + self.E(mu)
        return x_local, x_global


def _test():
    map_ = mapdata.RiskMap.load("mini.yaml", "images")
    # map_ = mapdata.RiskMap.load("standard.yaml", "images")
    ai = AIPlayer(32, 2, map_=map_)
    nps = sum(p.numel() for p in ai.parameters())
    print(f"{nps} parameters")
    batch_size = 5
    local = torch.zeros(batch_size, len(map_.territories), map_.max_players())
    global_ = torch.zeros(batch_size, GLOBAL_STATUS_LEN)
    y = ai(local, global_)
    print(y.size())


if __name__ == "__main__":
    _test()
